# Library application

Library sample application used for managing books. REST API is implemented in Spring Boot. Endpoints are documented with Swagger 2 and secured with role based Spring Security. Data is populated automatically in H2 embedded database on application startup. Application is tested with integration and unit tests.

To run docker image of this app run: 

docker login registry.gitlab.com

docker build -t registry.gitlab.com/miodragbrkljac/hibridnapraksaorganizacijarada/library-backend .

docker push registry.gitlab.com/miodragbrkljac/hibridnapraksaorganizacijarada/library-backend

docker run -p 8080:8080 registry.gitlab.com/miodragbrkljac/hibridnapraksaorganizacijarada/library-backend
